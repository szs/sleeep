sleeep
======

A better sleep

Usage
=====

.. code-block:: shell

                sleeep 2

See ``sleeep --help`` for a full list of options.

How to Contribute
=================

If you find a bug, want to propose a feature or need help getting this package to work with your data
on your system, please don't hesitate to file an `issue <https://gitlab.com/szs/sleeep/-/issues>`_ or write
an email. Merge requests are also much appreciated!

Project links
=============

* `Repository <https://gitlab.com/szs/sleeep>`_
* `Documentation <https://sleeep.readthedocs.io/en/latest/>`_
* `pypi page <https://pypi.org/project/sleeep/>`_
