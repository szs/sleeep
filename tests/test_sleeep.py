"""Tests for sleeep"""

from time import time

from sleeep import bar, check_term, run, styles


def test_bar():
    for i in range(100):
        assert bar(0, i) == ""
    for i in range(2, 100):
        assert bar(0, i) == ""
    for i in range(2, 100):
        assert bar(0.00001, i) != ""
    for i in range(2, 100):
        assert bar(1, i) == "█" * (i)
    for i in range(2, 100, 2):
        assert bar(0.5, i) == "█" * (i // 2)


def test_styles():
    for style in styles:
        assert len(bar(0.5, 10, style)) == 5


def test_run(capsys):
    status = run(["-n", "0.1"])
    assert status is None
    status = run(["-n", ".1s"])
    assert status is None
    status = run(["-n", ".00166m"])
    assert status is None
    status = run(["-n", ".00002777h"])
    assert status is None
    status = run(["-n", ".0000011574d"])
    assert status is None
    output = capsys.readouterr()
    assert output.out.split("|")[2].startswith("█")
    assert output.out.split("|")[-3].endswith("█")


def test_run_quiet(capsys):
    status = run([".5", "-q"])
    assert status is None
    output = capsys.readouterr()
    assert output.out.count("█") == 0, ">" + output.out + "<"


def test_run_transient(capsys):
    status = run(["-t", "-n", "1"])
    assert status is None
    output = capsys.readouterr()
    assert output.out.endswith("\r\x1b[K\x1b[?25h\x1b[0m"), ">" + output.out + "<"


def test_run_width(capsys):
    status = run(["-w 10", ".1"])
    assert status is None
    status = run(["-w 80", "-n", ".1"])
    assert status is None
    output = capsys.readouterr()
    assert len(output.out.split("|")[-3]) == 48, ">" + output.out.split("|")[-3] + "<"


def test_check_term():
    assert check_term("q") == 2
    assert check_term("f") == 0
    for x in "abcdeghijklmnoprstuvwxyz":
        assert check_term(x) is None


def test_run_with_style(capsys):
    run(["-n", "--style", "domino", "1"])
    output = capsys.readouterr()
    assert output.out.split("|")[2].startswith("█")
    assert output.out.split("|")[-3].endswith("█")


def test_duration():
    t0 = time()
    print()
    status = run(["1", "-f", "100"])
    assert status is None, status
    t1 = time()

    assert t1 - t0 > 0.99, f"run lasted {t1 - t0}"
    assert t1 - t0 < 1.015, f"run lasted {t1 - t0}"

    print(f"run lasted {t1 - t0}")
